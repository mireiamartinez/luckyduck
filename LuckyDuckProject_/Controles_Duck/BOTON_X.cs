﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Controls03
{
    public class BOTON_X : Button
    {
        public BOTON_X() 
        {
            InitializeComponent();
        }

        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BOTON_X));
            this.SuspendLayout();
            // 
            // BOTON_X
            // 
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.FlatAppearance.BorderSize = 0;
            this.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Size = new System.Drawing.Size(20, 20);
            this.Text = " ";
            this.UseVisualStyleBackColor = false;
            this.Click += new System.EventHandler(this.BOTON_X_Click);
            this.ResumeLayout(false);

        }

        private void BOTON_X_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        
    }
}
