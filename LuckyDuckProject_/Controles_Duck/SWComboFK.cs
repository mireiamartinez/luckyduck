﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data;
using AccesoDatos;

namespace Controls03
{
    public class SWComboFK : ComboBox
    {
        private String _nomTaula;
        public String nomTaula
        {
            get { return _nomTaula; }
            set
            {
                this._nomTaula = value;
            }
        }

        Class1 acceso = new Class1();
        public SWComboFK()
        {
            InitializeComponent();

        }
        
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // SWComboFK
            // 
            this.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.FormattingEnabled = true;
            this.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SelectedValueChanged += new System.EventHandler(this.SWComboFK_SelectedValueChanged);
            this.ResumeLayout(false);

        }

        //private String _ControlID;
        //public String ControlID
        //{
        //    get { return _ControlID; }
        //    set
        //    {
        //        this._ControlID = value;
        //    }
        //}

        private string _CampMostrar;
        public string CampMostrar
        {
            get { return this._CampMostrar; }
            set
            {
                this._CampMostrar = value;
            }
        }

        private string _CampId;
        public string CampId
        {
            get { return this._CampId; }
            set
            {
                this._CampId = value;
            }
        }

        public void CarregarCombo()
        {
            DataTable dt = acceso.PortarPerTaula(this.nomTaula);
            this.DataSource = dt;
            this.DisplayMember = this.CampMostrar;
            this.ValueMember = this.CampId;
           
        }

        private void SWComboFK_SelectedValueChanged(object sender, EventArgs e)
        {
            //Form form = this.FindForm();
            //foreach (Control ctr in form.Controls)
            //{
            //    if(ctr.GetType() == typeof(SWComboFK))
            //    {
            //        ctr.Focus();
            //    }
            //}

        }
    }
}
