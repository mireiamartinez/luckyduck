﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AccesoDatos;
using Controls03;
using Formularios;

namespace LuckyDuck_Project1
{
    public partial class Formulario : formulario_base
    {
        public Formulario()
        {
            InitializeComponent();
        }

        Class1 acceso = new Class1();

        private void Form4_Load(object sender, EventArgs e)
        {
            Info_Textbox(); 
        }

        private void Info_Textbox()
        {
            foreach (Control ctr in Controls)
            {
                if (ctr.GetType() == typeof(SWComboFK))
                {
                    SWComboFK ctr1 = (SWComboFK)ctr;
                    ctr1.CarregarCombo();
                    ctr1.DataBindings.Add("SelectedValue", dataGridView1.DataSource, ctr1.CampId);
                    ctr1.Validated += new System.EventHandler(this.ValidarComboBox);
                }
            }

            foreach (Control ctr in Controls)
            {
                if (ctr.GetType() == typeof(SWTextbox))
                {
                    ctr.DataBindings.Clear();
                    ctr.Text = string.Empty;
                    ctr.DataBindings.Add("Text", dataGridView1.DataSource, ctr.Tag.ToString());
                    ctr.Validated += new System.EventHandler(this.ValidarTextBox);
                }
            }
            
        }
        private void ValidarTextBox(object sender, EventArgs e)
        {
            ((SWTextbox)sender).DataBindings[0].BindingManagerBase.EndCurrentEdit();
        }

        private void ValidarComboBox(object sender, EventArgs e)
        {
            ((SWComboFK)sender).DataBindings[0].BindingManagerBase.EndCurrentEdit();
        }
        private void swTextbox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
